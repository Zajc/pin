package pl.sdacademy.day13.pin;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Pin {
    private static Scanner scanner = new Scanner(System.in);
    private static final int PIN = 12345;
    private static int counter = 3;

    public static void main(String[] args) {
        check();
    }

    private static void check() {
        counter = 3;
        while (counter != 0) {
            System.out.println("Podaj PIN: ");
            if (getNumber() == PIN) {
                System.out.println("Poprawny PIN");
                break;
            } else {
                System.out.println("Niepoprawny PIN");
                counter--;
            }

        }

    }

    protected static int getNumber() {
        int number;
        try {
            number = scanner.nextInt();
        } catch (InputMismatchException e) {
            if (counter != 0) {
                counter--;
                System.out.println("Podaj Pin: ");
                number = getNumber();
            } else {
                number = 0;
            }
        }
        return number;
    }
}
